﻿using Ninject;
using Ninject.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;

namespace MKAdmin.Injection
{
    /// <summary>
    ///  Provides a Ninject implementation of IDependencyScope
    /// which resolves services using the Ninject container.
    /// </summary>
    public class NinjectDependencyScope : IDependencyScope
    {
        /// <summary>
        /// 
        /// </summary>
        IResolutionRoot resolver;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resolver"></param>
        public NinjectDependencyScope(IResolutionRoot resolver)
        {
            this.resolver = resolver;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public object GetService(Type serviceType)
        {
            if (resolver == null)
                throw new ObjectDisposedException("this", "This scope has been disposed");

            return resolver.TryGet(serviceType);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public System.Collections.Generic.IEnumerable<object> GetServices(Type serviceType)
        {
            if (resolver == null)
                throw new ObjectDisposedException("this", "This scope has been disposed");

            return resolver.GetAll(serviceType);
        }
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            IDisposable disposable = resolver as IDisposable;
            if (disposable != null)
                disposable.Dispose();

            resolver = null;
        }
    }

    /// <summary>
    ///  This class is the resolver, but it is also the global scope
    /// so we derive from NinjectScope.
    /// </summary>
    public class ApiDependencyResolver : NinjectDependencyScope, IDependencyResolver
    {
        IKernel kernel;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="kernel"></param>
        public ApiDependencyResolver(IKernel kernel)
            : base(kernel)
        {
            this.kernel = kernel;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IDependencyScope BeginScope()
        {
            return new NinjectDependencyScope(kernel.BeginBlock());
        }
    }
}
