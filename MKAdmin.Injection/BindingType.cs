﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.Injection
{
    public class BindingType
    {
        public void Bind<T, K>(Ninject.IKernel kernel) where K : T
        {
            kernel.Bind<T>().To<K>();
        }
    }
}
