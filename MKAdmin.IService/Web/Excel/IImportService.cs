﻿using MKAdmin.DTO.Web.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.IService.Web.Excel
{
    public interface IImportService
    {
        /// <summary>
        /// 导入数据
        /// </summary>
        /// <returns></returns>
        Result ImportData(DataTable dtResult, UserInfoModel userInfo);
    }
}
