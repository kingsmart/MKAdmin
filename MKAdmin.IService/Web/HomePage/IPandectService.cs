﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MKAdmin.DTO.Web.Common;
using MKAdmin.DTO.Web.HomePage.Pandect;

namespace MKAdmin.IService.Web.HomePage
{
    public interface IPandectService
    {
        /// <summary>
        /// 获取首页汇总数据
        /// </summary>
        /// <returns></returns>
        Result<List<GetTabContentModel>> GetTabContent(GetTabContentParameter parameter);

        /// <summary>
        /// 好友区域统计
        /// </summary>
        /// <returns></returns>
        PagingList<FriendsAreaCollectModel> GetFriendAreaCollect(FriendsAreaCollectParameter parameter
           , UserInfoModel userInfo);

        /// <summary>
        /// 下载源码日志
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        Result DownLoadCodeLog(UserInfoModel userInfo);
    }
}
