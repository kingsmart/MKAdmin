
--用户账号信息表
If Not Exists (Select * From sysobjects Where name = 'OperatorInfo' And type = 'U')  
Begin
	/*==============================================================*/
	/* Table: OperatorInfo                                         */
	/*==============================================================*/
	create table OperatorInfo (
	   OperatorId          int                  identity(10000,1),
	   OperatorNo          nvarchar(20)         not null,
	   OperatorName        nvarchar(20)         not null,
	   OperatorPwd         nvarchar(50)         not null,
	   StatusCode          tinyint              not null,
	   Remark              nvarchar(200)        not null,
	   CreateTime          datetime             not null,
	   UpdateTime          datetime             not null,
	   constraint PK_OperatorInfo_OperatorId primary key (OperatorId)
	)
End
Go

--角色信息表
If Not Exists (Select * From sysobjects Where name = 'OperatorRoleInfo' And type = 'U')  
Begin
	/*==============================================================*/
	/* Table: OperatorRoleInfo                                    */
	/*==============================================================*/
	create table OperatorRoleInfo (
	   RoleId              int                  identity(10000,1),
	   RoleName            nvarchar(20)         not null,
	   RoleDesc          nvarchar(100)        not null,
	   StatusCode          tinyint              not null,
	   UpdatorId           int                  not null,
	   UpdateTime          datetime             not null,
	   CreatorId           int                  not null,
	   CreateTime          datetime             not null,
	   Remark               nvarchar(255)        not null,
	   constraint PK_OperatorRoleInfo_RoleId primary key (RoleId)
	)
	execute sp_addextendedproperty 'MS_Description', 
	   '1 启用
	   2 禁用
	   3 删除',
	   'user', 'dbo', 'table', 'OperatorRoleInfo', 'column', 'StatusCode'
End
Go

--用户账号角色配置表
If Not Exists (Select * From sysobjects Where name = 'OperatorRoleConfig' And type = 'U')  
Begin
	/*==============================================================*/
	/* Table: OperatorRoleConfig                                  */
	/*==============================================================*/
	create table OperatorRoleConfig (
	   ConfigId            int                  identity(10000,1),
	   OperatorId          int                  not null,
	   RoleId              int                  not null,
	   UpdateTime          datetime             not null,
	   constraint PK_OperatorRoleConfig_ConfigId primary key (ConfigId)
	)
End
Go

--权限
If Not Exists (Select * From sysobjects Where name = 'OperatorRightInfo' And type = 'U')  
Begin
	/*==============================================================*/
	/* Table: OperatorRightInfo                                     */
	/*==============================================================*/
	create table OperatorRightInfo (
	   RightId              int                  not null,
	   RightParentId        int                  not null,
	   IsLog                bit                  not null,
	   TypeCode             tinyint              not null,
	   StatusCode           tinyint              not null,
	   OrderNo              tinyint              not null,
	   RightName            nvarchar(20)         not null,
	   IconName             nvarchar(50)          not null,
	   ResourceName         varchar(100)         not null,
	   MenuPageUrl          nvarchar(100)        not null,
	   Remark               nvarchar(255)        not null,
	   constraint PK_OperatorRightInfo_RightId primary key (RightId)
	)
	execute sp_addextendedproperty 'MS_Description', 
	   '权限父ID',
	   'user', 'dbo', 'table', 'OperatorRightInfo', 'column', 'RightParentId'

	execute sp_addextendedproperty 'MS_Description', 
	   '是否日志记录',
	   'user', 'dbo', 'table', 'OperatorRightInfo', 'column', 'IsLog'

	execute sp_addextendedproperty 'MS_Description', 
	   '0、菜单权限；
	   1、操作权限；
	   2、数据权限',
	   'user', 'dbo', 'table', 'OperatorRightInfo', 'column', 'TypeCode'

	execute sp_addextendedproperty 'MS_Description', 
	   '针对菜单权限控制
	   0 显示
	   1 隐藏
	   2 删除',
	   'user', 'dbo', 'table', 'OperatorRightInfo', 'column', 'StatusCode'

	execute sp_addextendedproperty 'MS_Description', 
	   '排序顺序',
	   'user', 'dbo', 'table', 'OperatorRightInfo', 'column', 'OrderNo'

	execute sp_addextendedproperty 'MS_Description', 
	   '权限名称',
	   'user', 'dbo', 'table', 'OperatorRightInfo', 'column', 'RightName'

	execute sp_addextendedproperty 'MS_Description', 
	   'ICON图标名称',
	   'user', 'dbo', 'table', 'OperatorRightInfo', 'column', 'IconName'

	execute sp_addextendedproperty 'MS_Description', 
	   '资源名称',
	   'user', 'dbo', 'table', 'OperatorRightInfo', 'column', 'ResourceName'

	execute sp_addextendedproperty 'MS_Description', 
	   '备注',
	   'user', 'dbo', 'table', 'OperatorRightInfo', 'column', 'Remark'
End
Go

--角色-权限配置表
If Not Exists (Select * From sysobjects Where name = 'OperatorRoleRightConfig' And type = 'U')  
Begin
	/*==============================================================*/
	/* Table: OperatorRoleRightConfig                               */
	/*==============================================================*/
	create table OperatorRoleRightConfig (
	   ConfigId             int                  identity(10000,1),
	   RoleId               int                  not null,
	   RightId              int                  not null,
	   constraint PK_OperatorRoleRightConfig_ConfigId primary key (ConfigId)
	)
	execute sp_addextendedproperty 'MS_Description', 
	   '角色-权限配置表 OperatorRoleRightConfig',
	   'user', 'dbo', 'table', 'OperatorRoleRightConfig'
End
Go

--员工信息表
If Not Exists (Select * From sysobjects Where name = 'EmployeeInfo' And type = 'U')  
Begin
	/*==============================================================*/
	/* Table: EmployeeInfo                                          */
	/*==============================================================*/
	create table EmployeeInfo (
	   EmployeeId           int                  identity(10000,1),
	   EmployeeName         nvarchar(20)         not null,
	   EmployeeSex          tinyint              not null,
	   EmployeeAge          int                  not null,
	   EmployeeHeight       int                  not null,
	   EmployeeWeight       decimal(10,1)        not null,
	   PayMoney             decimal(10,2)        not null,
	   HeadFileName         nvarchar(200)        not null,
	   ContactPhone         nvarchar(20)         not null,
	   Education            nvarchar(20)         not null,
	   AddressDetail        nvarchar(200)        not null,
	   Hobby                nvarchar(200)        not null,
	   Motto                nvarchar(100)        not null,
	   StatusCode           tinyint              not null,
	   [Status]             tinyint              not null,
	   EntryTime            datetime             not null,
	   CreatorId		int		     not null,
	   CreateTime           datetime             not null,
	   UpdateTime           datetime             not null,
	   Remark               nvarchar(200)        not null,
	   constraint PK_EmployeeInfo_EmployeeId primary key (EmployeeId)
	)
	execute sp_addextendedproperty 'MS_Description', 
	   '1 男
	   2 女',
	   'user', 'dbo', 'table', 'EmployeeInfo', 'column', 'EmployeeSex'

	execute sp_addextendedproperty 'MS_Description', 
	   '0 正常
	   1 离职',
	   'user', 'dbo', 'table', 'EmployeeInfo', 'column', 'StatusCode'

	execute sp_addextendedproperty 'MS_Description', 
	   '0 正常
	   1 禁用
	   2 删除',
	   'user', 'dbo', 'table', 'EmployeeInfo', 'column', 'Status'
End
Go

--下载源码日志表
If Not Exists (Select * From sysobjects Where name = 'LoadSoundCodeLog' And type = 'U')  
Begin
	/*==============================================================*/
	/* Table: LoadSoundCodeLog                                      */
	/*==============================================================*/
	create table LoadSoundCodeLog (
	   LoadId               int                  identity(10000,1),
	   OperatorId           int					 not null,
	   IPAddress            nvarchar(30)         not null,
	   CreateTime           datetime             not null,
	   constraint PK_LOADSOUNDCODELOG primary key (LoadId)
	)
End
Go

--登录日志表
If Not Exists (Select * From sysobjects Where name = 'LoginLog' And type = 'U')  
Begin
	/*==============================================================*/
	/* Table: LoginLog                                              */
	/*==============================================================*/
	create table LoginLog (
	   LoginId              int                  identity(10000,1),
	   OperatorNo           nvarchar(20)         not null,
	   LoginStatus          bit                  not null,
	   IPAddress            nvarchar(30)         not null,
	   LoginTime            datetime             not null,
	   constraint PK_LOGINLOG primary key (LoginId)
	)
	execute sp_addextendedproperty 'MS_Description', 
	   'true 登录成功
	   false 登录失败',
	   'user', 'dbo', 'table', 'LoginLog', 'column', 'LoginStatus'
End
Go

--页面源码信息
If Not Exists (Select * From sysobjects Where name = 'PageSoundCodeRightInfo' And type = 'U')  
Begin
	/*==============================================================*/
	/* Table: PageSoundCodeRightInfo                                */
	/*==============================================================*/
	create table PageSoundCodeRightInfo (
	   Id                   int                  not null identity(10000,1),
	   RightId              int                  not null,
	   SoundCodeFileName    nvarchar(255)        not null,
	   OrderNo              tinyint              not null,
	   StatusCode           tinyint              null,
	   Remark               nvarchar(255)        not null,
	   constraint PK_PageSoundCodeRightInfo_Id primary key (Id)
	)
	execute sp_addextendedproperty 'MS_Description', 
	   '排序顺序 值越大排序越靠前',
	   'user', 'dbo', 'table', 'PageSoundCodeRightInfo', 'column', 'OrderNo'

	execute sp_addextendedproperty 'MS_Description', 
	   '0 显示
	   1 隐藏
	   2 删除',
	   'user', 'dbo', 'table', 'PageSoundCodeRightInfo', 'column', 'StatusCode'

	execute sp_addextendedproperty 'MS_Description', 
	   '备注',
	   'user', 'dbo', 'table', 'PageSoundCodeRightInfo', 'column', 'Remark'

End