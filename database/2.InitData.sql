-- delete from dbo.OperatorRightInfo

--初始化登录账号 admin 123456
IF NOT EXISTS(SELECT 1 FROM dbo.OperatorInfo WITH(NOLOCK) WHERE OperatorNo = 'admin')
BEGIN
	SET IDENTITY_INSERT OperatorInfo ON;
	Insert Into dbo.OperatorInfo(OperatorId,OperatorNo,OperatorName
				,OperatorPwd,StatusCode,Remark,CreateTime,UpdateTime)
			VALUES(1,'admin','管理员','F59BD65F7EDAFB087A81D4DCA06C4910',0,'',GETDATE(),GETDATE())
	SET IDENTITY_INSERT OperatorInfo OFF;
END

delete from OperatorRightInfo
--下拉框 , 联动下拉 , 可输入下拉提示 , 多选下拉
Insert Into dbo.OperatorRightInfo
	(RightId	,RightParentId,IsLog	,TypeCode	,StatusCode	,OrderNo	,RightName				,IconName												,ResourceName											,MenuPageUrl										,Remark)
VALUES(1000		,-1				,1		,0			,0			,10			,'表格管理'				,'/images/nav_table.png'								,''														,''													,'')	
	,(1100		,-1				,1		,0			,0			,20			,'表单'				    ,'/images/form.png'										,''														,''													,'')
	,(1200		,-1				,1		,0			,0			,30			,'图表'					,'/images/nav_chart.png'								,''														,''													,'')			
	,(1300		,-1				,1		,0			,0			,40			,'Excel操作'			,'/images/nav_manage.png'								,''														,''													,'')			
	,(1400		,-1				,1		,0			,0			,90			,'文件管理'				,'/images/nav_file.png'							        ,''														,''													,'')		
	,(1500		,-1				,1		,0			,0			,100		,'通讯管理'				,'/images/nav_wx.png'							        ,''														,''													,'')		

	,(100000	,1000			,1		,0			,0			,10			,'基本表格'				,''														,'table/basictable/list'								,'table/basictable/index'							,'表格控件')
	,(100010	,1000			,1		,0			,0			,20			,'复选框表格1'			,''														,'table/checkboxtable/list'								,'table/checkboxtable/index'						,'表格控件')
	,(100015	,1000			,1		,0			,0			,25			,'复选框表格2'			,''														,'table/checkboxtable/list'								,'table/checkboxtable/multiIndex'					,'表格控件')
	,(100020	,1000			,1		,0			,0			,30			,'设置单元格样式'   	,''														,'table/cellstyle/list'									,'table/cellstyle/index'							,'表格控件')

	,(110000	,1100			,1		,0			,0			,10			,'下拉框'				,''														,'form/dropdown/list'									,'form/dropdown/index'								,'表单-下拉框')
	,(110010	,1100			,1		,0			,0			,20			,'CheckBox复选框'		,''														,'form/checkbox/list'									,'form/checkbox/index'								,'表单-checkbox复选框')

	,(120000	,1200			,1		,0			,0			,10			,'折线图'				,''														,'chart/line/list'										,'chart/line/index'									,'图表')
	,(120010	,1200			,1		,0			,0			,20			,'柱状图'				,''														,'chart/columnar/list'									,'chart/columnar/index'								,'图表')
	,(120020	,1200			,1		,0			,0			,30			,'饼图'					,''														,'chart/pie/list'										,'chart/pie/index'									,'图表')

	,(130000	,1300			,1		,0			,0			,10			,'导入Excel'			,''														,'excel/import/list'									,'excel/import/index'								,'Excel操作')
	,(130010	,1300			,1		,0			,0			,20			,'导出Excel'			,''														,'excel/export/list'									,'excel/export/index'								,'Excel操作')

	,(140000	,1400			,1		,0			,0			,10			,'文件上传'				,''														,'file/upload/list'										,'file/upload/index'								,'文件管理')
	,(140010	,1400			,1		,0			,0			,20			,'在线预览'				,''														,'file/preview/list'									,'file/preview/index'								,'文件管理')
	,(140020	,1400			,1		,0			,0			,30			,'图片裁剪上传'			,''														,'file/crop/list'										,'file/crop/index'									,'文件管理')

	,(150000	,1500			,1		,0			,0			,10			,'发送邮件'				,''														,'communicate/email/list'								,'communicate/email/index'							,'通讯管理')
	,(150010	,1500			,1		,0			,0			,20			,'在线聊天'				,''														,'communicate/chat/list'								,'communicate/chat/index'							,'通讯管理')

	,(10000000	,100000			,1		,1			,0			,10			,'显示模块'				,''														,''														,''													,'')				

	,(10001000	,100010			,1		,1			,0			,10			,'显示模块'				,''														,''														,''													,'')				

	,(10002000	,100020			,1		,1			,0			,10			,'显示模块'				,''														,''														,''													,'')

	,(10003000	,100030			,1		,1			,0			,10			,'显示模块'				,''														,''														,''													,'')

	,(10004000	,100040			,1		,1			,0			,10			,'显示模块'				,''														,''														,''													,'')

	,(10005000	,100050			,1		,1			,0			,10			,'显示模块'				,''														,''														,''													,'')	

	,(10006000	,100060			,1		,1			,0			,10			,'显示模块'				,''														,''														,''													,'')	

--select * from dbo.PageSoundCodeRightInfo


Delete from PageSoundCodeRightInfo
Insert Into dbo.PageSoundCodeRightInfo(RightId,SoundCodeFileName,OrderNo,StatusCode,Remark)
								Values(0,'/Views/Home/Index.cshtml',100,0,''),(0,'/Views/Home/Welcome.cshtml',90,0,''),(0,'/Scripts/Home/homeIndex.js',80,0,'')
									 ,(100000,'/Views/Table/BasicTable/Index.cshtml',100,0,''),(100000,'/Views/Table/BasicTable/Dialog/Add.cshtml',90,0,''),(100000,'/Scripts/Table/basicTableIndex.js',80,0,'')
									 ,(100010,'/Views/Table/CheckBoxTable/Index.cshtml',100,0,''),(100010,'/Scripts/Table/checkBoxTableIndex.js',90,0,'')
									 ,(100020,'/Views/Table/CellStyle/Index.cshtml',100,0,''),(100020,'/Scripts/Table/cellStyleIndex.js',90,0,'')
									 ,(110000,'/Views/Form/DropDown/Index.cshtml',100,0,''),(110000,'/Scripts/Form/dropDownIndex.js',90,0,'')
									 ,(110010,'/Views/Form/CheckBox/Index.cshtml',100,0,''),(110010,'/Scripts/Form/checkBoxIndex.js',90,0,'')
									 ,(120000,'/Views/Chart/Line/Index.cshtml',100,0,''),(120000,'/Scripts/Chart/chartLineIndex.js',90,0,'')
									 ,(120010,'/Views/Chart/Columnar/Index.cshtml',100,0,''),(120010,'/Scripts/Chart/chartColumnarIndex.js',90,0,'')
									 ,(120020,'/Views/Chart/Pie/Index.cshtml',100,0,''),(120020,'/Scripts/Chart/chartPieIndex.js',90,0,'')
									 ,(130000,'/Views/Excel/Import/Index.cshtml',100,0,''),(130000,'/Scripts/Excel/excelImportIndex.js',90,0,'')
									 ,(130010,'/Views/Excel/Export/Index.cshtml',100,0,''),(130010,'/Scripts/Excel/excelExportIndex.js',90,0,'')
									 ,(140000,'/Views/File/Upload/Index.cshtml',100,0,''),(140000,'/Scripts/File/fileUploadIndex.js',90,0,'')
									 ,(140010,'/Views/File/Preview/Index.cshtml',100,0,''),(140010,'/Scripts/File/filePreviewIndex.js',90,0,'')
									 ,(140020,'/Views/File/Crop/Index.cshtml',100,0,''),(140020,'/Scripts/File/fileCropIndex.js',90,0,'')
									 ,(150000,'/Views/Communicate/Email/Index.cshtml',100,0,''),(150000,'/Scripts/Communicate/communicateEmailIndex.js',90,0,'')
									 ,(150010,'/Views/Communicate/Chat/Index.cshtml',100,0,''),(150010,'/Scripts/Communicate/communicateChatIndex.js',90,0,'')
									 
	