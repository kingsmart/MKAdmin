﻿using MKAdmin.DataHelper;
using MKAdmin.DTO.Web.Common;
using MKAdmin.IService.Web.Excel;
using MKAdmin.ToolKit;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.ServiceImpl.Web.Excel
{
    public class ImportService : IImportService
    {
        /// <summary>
        /// 导入数据
        /// </summary>
        /// <returns></returns>
        public Result ImportData(DataTable dtResult, UserInfoModel userInfo)
        {
            var result = new Result();

            try
            {
                using (var util = new DBHelper())
                {
                    DataTable dbTable = new DataTable();
                    dbTable.TableName = "EmployeeInfo";

                    dbTable.Columns.Add("EmployeeName", typeof(string));
                    dbTable.Columns.Add("EmployeeAge", typeof(byte));
                    dbTable.Columns.Add("EmployeeSex", typeof(byte));
                    dbTable.Columns.Add("EmployeeHeight", typeof(int));

                    dbTable.Columns.Add("Education", typeof(string));
                    dbTable.Columns.Add("ContactPhone", typeof(string));
                    dbTable.Columns.Add("EmployeeWeight", typeof(decimal));
                    dbTable.Columns.Add("PayMoney", typeof(decimal));

                    dbTable.Columns.Add("HeadFileName", typeof(string));
                    dbTable.Columns.Add("AddressDetail", typeof(string));
                    dbTable.Columns.Add("Hobby", typeof(string));
                    dbTable.Columns.Add("Motto", typeof(string));

                    dbTable.Columns.Add("StatusCode", typeof(byte));
                    dbTable.Columns.Add("EntryTime", typeof(DateTime));
                    dbTable.Columns.Add("CreateTime", typeof(DateTime));
                    dbTable.Columns.Add("UpdateTime", typeof(DateTime));

                    dbTable.Columns.Add("Remark", typeof(string));
                    dbTable.Columns.Add("CreatorId", typeof(int));
                    dbTable.Columns.Add("Status", typeof(byte));

                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        string employeeName = dtResult.Rows[i]["姓名"].ToString();
                        string employeeAge = dtResult.Rows[i]["年龄"].ToString();
                        string employeeSex = dtResult.Rows[i]["性别"].ToString();
                        string employeeHeight = dtResult.Rows[i]["身高"].ToString();
                        string education = dtResult.Rows[i]["学历"].ToString();
                        string contactPhone = dtResult.Rows[i]["联系方式"].ToString();

                        int _employeeSex = 0;
                        if (employeeSex.Trim() == "男")
                        {
                            _employeeSex = 1;
                        }
                        else if (employeeSex.Trim() == "女")
                        {
                            _employeeSex = 2;
                        }
                        dbTable.Rows.Add(employeeName, employeeAge, _employeeSex, employeeHeight
                            , education, contactPhone, 0, 1000
                            , "", "", "", "梦想不一定能实现，但一定要有"
                            , 0, DateTime.Now, DateTime.Now, DateTime.Now
                            , "", userInfo.OperatorId, 0);
                    }

                    util.BulkInserts(dbTable, "EmployeeInfo");

                    result.status = true;
                    result.msg = "导入成功";
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error("导入数据异常", ex);
                result.status = false;
                result.msg = "导入失败,请稍后重试...";
            }

            return result;
        }
    }
}
