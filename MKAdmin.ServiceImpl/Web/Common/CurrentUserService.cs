﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MKAdmin.DataHelper;
using MKAdmin.DTO.Web.Common;
using MKAdmin.DTO.Web.Login;
using MKAdmin.ToolKit;
using MKAdmin.ToolKit.CacheHelper;

namespace MKAdmin.ServiceImpl.Web.Common
{
    public class CurrentUserService
    {
        /// <summary>
        /// 获取客服账号字符('chat001','chat002','chat003')
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="ContainCurrentAccount">是否包含当前账号</param>
        /// <returns></returns>
        public static Result<string> GetKfAccountStr(GetSubAccountParameter parameter, bool ContainCurrentAccount = true)
        {
            var result = new Result<string>() { status = false };

            List<AgentAccountModel> kfAccountList = GetKfAccountList(parameter, ContainCurrentAccount).data;
            if (kfAccountList != null && kfAccountList.Count > 0)
            {
                result.status = true;
                result.data = $"'{string.Join("','", kfAccountList.Select(q => q.AgentName))}'";
            }

            return result;
        }

        /// <summary>
        /// 获取客服账号列表('chat001','chat002','chat003')
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="ContainCurrentAccount">是否包含当前账号</param>
        /// <returns></returns>
        public static Result<List<AgentAccountModel>> GetKfAccountList(GetSubAccountParameter parameter,
            bool ContainCurrentAccount = true)
        {
            var result = new Result<List<AgentAccountModel>>() { status = false };

            //var cacheName = $"{CacheNameSetting.UserKfAccount}{userInfo.AgentId}";
            //List<AgentAccountModel> kfAccountList = RedisHelper.Get<List<AgentAccountModel>>(cacheName);
            List<AgentAccountModel> kfAccountList = new List<AgentAccountModel>();
            if (kfAccountList == null || kfAccountList.Count <= 0)
            {
                Result<List<AgentAccountModel>> list = new Login.LoginService().GetKfAccountList(parameter);
                if (list.status && list.data != null && list.data.Count > 0)
                {
                    kfAccountList = list.data;
                    //添加缓存
                    //RedisHelper.Set(cacheName, list.data);
                }
            }
            if (kfAccountList != null)
            {
                if (!ContainCurrentAccount)
                {
                    kfAccountList = kfAccountList.Where(q => q.AgentId != parameter.AgentId).ToList();
                }
                result.status = true;
                result.data = kfAccountList;
            }

            return result;
        }

        /// <summary>
        /// 获取自己和下级账号字符('mk','mk001','mk002')
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="ContainCurrentAccount">是否包含当前账号</param>
        /// <returns></returns>
        public static Result<string> GetMainAndSubAccountStr(GetSubAccountParameter parameter, bool ContainCurrentAccount = true)
        {
            var result = new Result<string>() { status = false };

            List<AgentAccountModel> mainAndSubList = GetMainAndSubAccountList(parameter, ContainCurrentAccount).data;
            if (mainAndSubList != null && mainAndSubList.Count > 0)
            {
                result.status = true;
                result.data = $"'{string.Join("','", mainAndSubList.Select(q => q.AgentName))}'";
            }

            return result;
        }

        /// <summary>
        /// 获取自己和下级账号列表('mk','mk001','mk002')
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="ContainCurrentAccount">是否包含当前账号</param>
        /// <returns></returns>
        public static Result<List<AgentAccountModel>> GetMainAndSubAccountList(GetSubAccountParameter parameter
            , bool ContainCurrentAccount = true)
        {
            var result = new Result<List<AgentAccountModel>>() { status = false };

            //var cacheName = $"{CacheNameSetting.UserMainAndSubAccount}{userInfo.AgentId}";
            //List<AgentAccountModel> mainAndSubList = RedisHelper.Get<List<AgentAccountModel>>(cacheName);
            List<AgentAccountModel> mainAndSubList = new List<AgentAccountModel>();
            if (mainAndSubList == null || mainAndSubList.Count <= 0)
            {
                Result<List<AgentAccountModel>> list = new Login.LoginService().GetMainAndSubAccountList(parameter);
                if (list.status && list.data != null && list.data.Count > 0)
                {
                    mainAndSubList = list.data;
                    //添加缓存
                    //RedisHelper.Set(cacheName, list.data);
                }
            }
            if (mainAndSubList != null)
            {
                if (!ContainCurrentAccount)
                {
                    mainAndSubList = mainAndSubList.Where(q => q.AgentId != parameter.AgentId).ToList();
                }
                result.status = true;
                result.data = mainAndSubList;
            }

            return result;
        }

        /// <summary>
        /// 获取当前超级管理员下账号列表
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="ContainCurrentAccount"></param>
        /// <returns></returns>
        public static Result<string> GetAccountStr(GetAccountListParameter parameter)
        {
            var result = new Result<string>() { status = false };

            List<AgentAccountModel> accountList = GetAccountList(parameter).data;
            if (accountList != null && accountList.Count > 0)
            {
                result.status = true;
                result.data = $"'{string.Join("','", accountList.Select(q => q.AgentName))}'";
            }

            return result;
        }

        /// <summary>
        /// 获取当前超级管理员下账号列表
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="ContainCurrentAccount"></param>
        /// <returns></returns>
        public static Result<List<AgentAccountModel>> GetAccountList(GetAccountListParameter parameter)
        {
            var result = new Result<List<AgentAccountModel>>() { status = false };

            //var cacheName = $"{CacheNameSetting.UserMainAndSubAccount}{userInfo.AgentId}";
            //List<AgentAccountModel> mainAndSubList = RedisHelper.Get<List<AgentAccountModel>>(cacheName);
            List<AgentAccountModel> accountList = new List<AgentAccountModel>();
            if (accountList == null || accountList.Count <= 0)
            {
                Result<List<AgentAccountModel>> list = new Login.LoginService().GetAccountList(parameter);
                if (list.status && list.data != null && list.data.Count > 0)
                {
                    accountList = list.data;
                    //添加缓存
                    //RedisHelper.Set(cacheName, list.data);
                }
            }
            if (accountList != null)
            {
                if (!parameter.ContainCurrentAccount)
                {
                    accountList = accountList.Where(q => q.AgentId != parameter.AgentId).ToList();
                }
                result.status = true;
                result.data = accountList;
            }

            return result;
        }

        public static Result<GetSubAccountParameter> ConvertToSubAccountParameter(UserInfoModel userInfo,
            EmAccountStatusCode accountStatusCode)
        {
            var result = new Result<GetSubAccountParameter>()
            {
                data = new GetSubAccountParameter()
            };

            result.data.AgentId = userInfo.OperatorId;
            result.data.AccountStatusCode = accountStatusCode;

            return result;
        }
    }
}
