﻿using MKAdmin.DTO.Web.Common;
using MKAdmin.ToolKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.ThirdParty.XcxQrCode
{
    public class XcxQrcodeHelper
    {
        /// <summary>
        /// 生成小程序二维码
        /// </summary>
        /// <returns></returns>
        public static Result<GetQrcodeIdInfoModel> CreateXcxQrcode()
        {
            var result = new Result<GetQrcodeIdInfoModel>();
            string data = WebHelper.SubmitForm($@"{AppSetting.LocationApiDomain}/QrcodeMgr/CreateXcxQrcode", null, "Post");
            if (!string.IsNullOrEmpty(data))
            {
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<Result<GetQrcodeIdInfoModel>>(data);
            }

            return result;
        }

        /// <summary>
        /// 获取小程序二维码访问状态
        /// </summary>
        /// <param name="qrcodeId"></param>
        /// <returns></returns>
        public static Result<int> GetQrcodeVisitInfo(int qrcodeId)
        {
            var result = new Result<int>();
            string data = WebHelper.SubmitForm($@"{AppSetting.LocationApiDomain}/QrcodeMgr/GetQrcodeVisitInfo?qrcodeId={qrcodeId}", null, "Get");
            if (!string.IsNullOrEmpty(data))
            {
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<Result<int>>(data);
            }

            return result;
        }
    }

    public class GetQrcodeIdInfoModel
    {
        /// <summary>
        /// 二维码Id
        /// </summary>
        public int QrcodeId { get; set; }
        /// <summary>
        /// 二维码图片
        /// </summary>
        public string QrcodeUrl { get; set; }
    }
}
