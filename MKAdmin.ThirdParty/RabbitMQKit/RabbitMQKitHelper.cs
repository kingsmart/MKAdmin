﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.ThirdParty.RabbitMQKit
{
    public class RabbitMQKitHelper
    {
        public static void SendMsg()
        {
            var connection = SingletonConnectionFactory.GetInstance();

            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(
                    queue: "UserMsg",
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                string message = string.Format("我是第 1 条消息");
                var body = Encoding.UTF8.GetBytes(message);

                //设置消息持久化
                IBasicProperties properties = channel.CreateBasicProperties();
                properties.DeliveryMode = 2;

                channel.BasicPublish(exchange: "", routingKey: "UserMsg", basicProperties: properties, body: body);
            }
        }
    }
}
