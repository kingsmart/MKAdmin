﻿using MKAdmin.Web.App_Start;
using MKAdmin.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MKAdmin.Web.Areas.Chart.Controllers
{
    /// <summary>
    /// 折线图
    /// </summary>
    public class LineController : BaseMvcController
    {
        // GET: Chart/Line
        public ActionResult Index()
        {
            return View(PageViewFilesConfig.ChartLineIndex);
        }
    }
}