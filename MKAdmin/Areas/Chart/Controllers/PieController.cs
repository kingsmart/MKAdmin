﻿using MKAdmin.Web.App_Start;
using MKAdmin.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MKAdmin.Web.Areas.Chart.Controllers
{
    /// <summary>
    /// 饼图
    /// </summary>
    public class PieController : BaseMvcController
    {
        // GET: Chart/Pie
        public ActionResult Index()
        {
            return View(PageViewFilesConfig.ChartPieIndex);
        }
    }
}