﻿using MKAdmin.IService.Web.Table;
using MKAdmin.Web.App_Start;
using MKAdmin.Web.Controllers;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MKAdmin.Web.Areas.Table.Controllers
{
    public class CellStyleController : BaseMvcController
    {
        [Inject]
        public ICellStyleService CellStyleService { set; get; }

        // GET: Table/CellStyle
        public ActionResult Index()
        {
            return View(PageViewFilesConfig.CellStyleIndex);
        }
    }
}