﻿using MKAdmin.IService.Web.Table;
using MKAdmin.Web.App_Start;
using MKAdmin.Web.Controllers;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MKAdmin.Web.Areas.Table.Controllers
{
    public class CheckBoxTableController : BaseMvcController
    {
        [Inject]
        public ICheckBoxTableService CheckBoxTableService { set; get; }

        // GET: Table/CheckBoxTable
        public ActionResult Index()
        {
            return View(PageViewFilesConfig.CheckBoxTableIndex);
        }

        public ActionResult MultiIndex()
        {
            return View(PageViewFilesConfig.CheckBoxTableMultiIndex);
        }
    }
}