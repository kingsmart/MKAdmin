﻿using MKAdmin.Web.Authorizes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MKAdmin.Web.App_Start;
using MKAdmin.DTO.Web.Common;
using MKAdmin.DTO.Web.Login;
using Ninject;
using MKAdmin.IService.Web.Login;
using MKAdmin.ToolKit;
using MKAdmin.Web.Filters;
using Newtonsoft.Json;
using MKAdmin.ThirdParty.RabbitMQKit;
using MKAdmin.ThirdParty.XcxQrCode;

namespace MKAdmin.Web.Controllers
{
    [LoginAuthorize]
    public class LoginController : Controller
    {
        [Inject]
        public ILoginService LoginService { set; get; }

        [HttpGet]
        public ActionResult Index()
        {
            LoginInfoParameter model = new LoginInfoParameter();
            HttpCookie cookie = Request.Cookies["loginCookie"];
            if (cookie != null)
            {
                LoginInfoParameter cookieModel = JsonConvert.DeserializeObject<LoginInfoParameter>(cookie.Value);
                if (cookieModel != null)
                {
                    model = cookieModel;
                    model.OperatorPwd = DESEncryptHelper.Decrypt(cookieModel.OperatorPwd);
                }
            }
            return View(PageViewFilesConfig.Login, model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult Index(LoginInfoParameter parameter)
        {
            if (!string.IsNullOrEmpty(parameter.OperatorNo))
            {
                if (!string.IsNullOrEmpty(parameter.OperatorPwd))
                {
                    var loginInfo = LoginService.UserLogin(parameter);

                    //记录登录日志
                    LoginService.UserLoginLog(parameter.OperatorNo, loginInfo.status);
                    if (loginInfo.status)
                    {
                        UserInfoModel info = new UserInfoModel();
                        info.OperatorId = loginInfo.data.OperatorId;
                        info.OperatorNo = loginInfo.data.OperatorNo;
                        info.OperatorName = loginInfo.data.OperatorName;


                        HttpCookie cookie = new HttpCookie("loginCookie");
                        LoginInfoParameter cookieModel = parameter;
                        cookieModel.OperatorPwd = DESEncryptHelper.Encrypt(parameter.OperatorPwd);
                        cookie.Value = JsonConvert.SerializeObject(cookieModel);

                        if (parameter.IsRemember)
                        {
                            cookie.Expires = DateTime.Now.AddDays(7);
                        }
                        else
                        {
                            cookie.Expires = DateTime.Now.AddDays(-1);
                        }
                        System.Web.HttpContext.Current.Response.Cookies.Add(cookie);

                        Loginer.SignIn(info, false);

                        return Redirect(FormsAuthentication.DefaultUrl);
                    }
                    else
                    {
                        ModelState.AddModelError(parameter.KeyValidateMessage, "登录名或密码错误");
                    }
                }
                else
                {
                    ModelState.AddModelError(parameter.KeyValidateMessage, "请输入密码");
                }
            }
            else
            {
                ModelState.AddModelError(parameter.KeyValidateMessage, "请输入登录名");
            }

            return View("~/Views/Login/Index.cshtml", parameter);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult SignOut()
        //{
        //    Loginer.SignOut();
        //    return Redirect(FormsAuthentication.LoginUrl);
        //}

    }
}