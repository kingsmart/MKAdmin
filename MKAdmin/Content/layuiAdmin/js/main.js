layui.use(['layer', 'form', 'element', 'jquery'], function () {
    var layer = layui.layer;
    var element = layui.element;
    var form = layui.form;
    var $ = layui.jquery;
    var hideBtn = $('#hideBtn');
    var mainLayout = $('#main-layout');
    var mainMask = $('.main-mask');

    //监听导航点击
    element.on('nav(leftNav)', function (elem) {

        if (elem == null || elem.length <= 0) {
            return;
        }

        var navA = $(elem[0]);
        var id = navA.attr('data-id');
        console.log(id)
        var url = navA.attr('data-url');
        var text = navA.attr('data-text');
        if (!url) {
            return;
        }
        var isActive = $('.main-layout-tab .layui-tab-title').find("li[lay-id=" + id + "]");
        if (isActive.length > 0) {

            //切换到选项卡
            element.tabChange('tab', id);
            console.log('id:' + id)
        } else {
            //获取tab个数
            var tabCount = $('.main-layout-body .main-layout-tab .layui-tab-title li').length;
            if (tabCount > 5) {
                layer.alert('请先关闭一些选项卡（最多允许打开5个）', {
                    icon: 5
                });
            }
            else {
                element.tabAdd('tab', {
                    title: text,
                    content: '<iframe src="' + url + '" id="menu_iframe_' + id + '" name="iframe' + id + '" class="iframe" framborder="0" data-id="' + id + '" scrolling="auto" width="100%"  height="100%"></iframe>',
                    id: id
                });
                element.tabChange('tab', id);

            }
        }
        $('.main-layout-body .layui-tab-title li', window.parent.document).click(function () {

            var tab_lay_this = $(this).attr('lay-id')
            var lay_li_ln = $('.layui-nav-child', window.parent.document).find('a')
            var tab_lay_ln = $('.layui-tab-title li', window.parent.document)
            console.log(tab_lay_this)
            $.each(lay_li_ln, function (i, item) {
                var currentId = $(item).attr('data-id')
                if (tab_lay_this == currentId) {
                    //$('.layui-nav-child', window.parent.document).find('a').parents('.layui-nav-item').removeClass('layui-nav-itemed')
                    $('.layui-nav-child', window.parent.document).find('a').parents('dd').removeClass('layui-this')
                    $('.layui-nav-child', window.parent.document).find('a').eq(i).parents('.layui-nav-item').addClass('layui-nav-itemed')
                    $('.layui-nav-child', window.parent.document).find('a').eq(i).parents('dd').addClass('layui-this')
                } else if (tab_lay_this == undefined) {
                    //$('.layui-nav-child', window.parent.document).find('a').parents('.layui-nav-item').removeClass('layui-nav-itemed')
                    $('.layui-nav-child', window.parent.document).find('a').parents('dd').removeClass('layui-this')
                }

            })

        })
        mainLayout.removeClass('hide-side');
        $('.main-layout-body .main-layout-tab .layui-tab-close', window.parent.document).click(function () {
            var ln = $(this).parents('li').attr('lay-id')
            var lay_id = $('.main-layout-tab', window.parent.document).find('.layui-this').attr('lay-id')
            var lay_this = $('.layui-nav-child', window.parent.document).find('a')

            $.each(lay_this, function (i, item) {
                var currentId = $(item).attr('data-id')

                if (lay_id == currentId) {

                    //$('.layui-nav-child', window.parent.document).find('a').parents('.layui-nav-item').removeClass('layui-nav-itemed')
                    $('.layui-nav-child', window.parent.document).find('a').parents('dd').removeClass('layui-this')
                    $('.layui-nav-child', window.parent.document).find('a').eq(i).parents('.layui-nav-item').addClass('layui-nav-itemed')
                    $('.layui-nav-child', window.parent.document).find('a').eq(i).parents('dd').addClass('layui-this')
                } else if (lay_id == undefined) {
                    //$('.layui-nav-child', window.parent.document).find('a').parents('.layui-nav-item').removeClass('layui-nav-itemed')
                    $('.layui-nav-child', window.parent.document).find('a').parents('dd').removeClass('layui-this')
                }

            })


        })
    });
    //监听导航点击

    element.on('nav(rightNav)', function (elem) {
        if (elem == null || elem.length <= 0) {
            return;
        }
        var navA = $(elem[0]);

        var id = navA.attr('data-id');
        var url = navA.attr('data-url');
        var text = navA.attr('data-text');
        if (!url) {
            return;
        }
        var isActive = $('.main-layout-tab .layui-tab-title').find("li[lay-id=" + id + "]");
        if (isActive.length > 0) {
            //切换到选项卡
            element.tabChange('tab', id);
        } else {
            element.tabAdd('tab', {
                title: text,
                content: '<iframe src="' + url + '" id="menu_iframe_' + id + '" name="iframe' + id + '" class="iframe" framborder="0" data-id="' + id + '" scrolling="auto" width="100%"  height="100%"></iframe>',
                id: id
            });
            element.tabChange('tab', id);

        }
        mainLayout.removeClass('hide-side');


    });
    //菜单隐藏显示
    hideBtn.on('click', function () {

        if (!mainLayout.hasClass('hide-side')) {
            mainLayout.addClass('hide-side');
        } else {
            mainLayout.removeClass('hide-side');
        }
    });
    //遮罩点击隐藏
    mainMask.on('click', function () {
        mainLayout.removeClass('hide-side');
    })



    //示范一个公告层
    //	layer.open({
    //		  type: 1
    //		  ,title: false //不显示标题栏
    //		  ,closeBtn: false
    //		  ,area: '300px;'
    //		  ,shade: 0.8
    //		  ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
    //		  ,resize: false
    //		  ,btn: ['火速围观', '残忍拒绝']
    //		  ,btnAlign: 'c'
    //		  ,moveType: 1 //拖拽模式，0或者1
    //		  ,content: '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;">后台模版1.1版本今日更新：<br><br><br>数据列表页...<br><br>编辑删除弹出功能<br><br>失去焦点排序功能<br>数据列表页<br>数据列表页<br>数据列表页</div>'
    //		  ,success: function(layero){
    //		    var btn = layero.find('.layui-layer-btn');
    //		    btn.find('.layui-layer-btn0').attr({
    //		      href: 'http://www.layui.com/'
    //		      ,target: '_blank'
    //		    });
    //		  }
    //		});
});