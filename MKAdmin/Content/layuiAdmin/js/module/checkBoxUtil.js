﻿//表格分页复选框
layui.define(['jquery', 'form'], function (exports) {
    var $ = layui.jquery
        , form = layui.form;

    var node_html = '';
    var checkBoxUtil = {
        /*初始化checkbox设置*/
        init: function (settings) {
            var param = {
                //需要生成checkbox的元素id
                nodeId: ''
                //checkbox节点集合
                , nodeList: []
            };
            $.extend(param, settings);

            getChildrenNodeHtml(param.nodeList);

            $("#" + param.nodeId).append(node_html);
            form.render();

            //设置节点展开，折叠
            foldNodeSetting();

            //监听节点点击事件
            monitorNodeEvent(param.nodeList, param.nodeId);
        }
        //获取当前获取的所有集合值
        , getCheckedVal: function (settings) {
            var param = {
                //表格id
                nodeId: ''
            };
            $.extend(param, settings);
            //debugger
            var checkedNodeList = [];
            $('#' + param.nodeId + ' input[type="checkbox"]:checked').each(function () {
                checkedNodeList.push($(this).val());
            });

            return checkedNodeList;
        }
    };

    //获取节点（三级节点）
    var getChildrenNodeHtml = function (nodeList) {
        //一级节点
        $.each(nodeList, function (index_first, item_first) {
            node_html += '<div class="ck-node-first ck-node-first-' + item_first.id + '" style="display: table;margin-top:10px;" >';
            //if (item.children != null && item.children.length > 0) {
            //debugger;
            var checkedFirst = item_first.checked ? 'checked' : '';
            node_html += '<img class="cs_cb_node_fold" data-fold=0 style="margin-top: -6px;margin-right: 10px;cursor:pointer"  src="/images/unfold.png"/>'
                + '  <input type="checkbox" ' + checkedFirst + ' name="" value="' + item_first.id + '" lay-filter="lf_node_' + item_first.id + '" title="' + item_first.name + '" lay-skin="primary">';

            if (item_first.children != null && item_first.children.length >= 0) {
                //二级节点
                $.each(item_first.children, function (index_second, item_second) {
                    node_html += '<div class="ck-node-second ck-node-second-' + item_second.id + '" style="display: table;margin-left: 20px;margin-top:10px;">';
                    // 
                    var checkedSecond = item_second.checked ? 'checked' : '';
                    node_html += '<img class="cs_cb_node_fold" data-fold=0 style="margin-top: -6px;margin-right: 10px;cursor:pointer" src="/images/unfold.png"/>'
                        + '<input type="checkbox" ' + checkedSecond + ' name="" value="' + item_second.id + '" lay-filter="lf_node_' + item_second.id + '" title="' + item_second.name + '" lay-skin="primary">';

                    if (item_second.children != null && item_second.children.length >= 0) {
                        //三级
                        node_html += '<div class="ck-node-third" style="display: table;margin-left: 40px;margin-top:10px;">';
                        $.each(item_second.children, function (index_third, item_third) {
                            //console.log(item_third);
                            var checkedThird = item_third.checked ? 'checked' : '';
                            node_html += '<input type="checkbox" ' + checkedThird + ' name="" value="' + item_third.id + '" lay-filter="lf_node_' + item_third.id + '" title="' + item_third.name + '" lay-skin="primary">';
                        });
                        //三级节点结束
                        node_html += '</div>';
                    }
                    //二级节点结束
                    node_html += '</div>';
                });
            }
            //一级节点结束
            node_html += '</div>';
        });
    }

    var foldNodeSetting = function () {
        $(".cs_cb_node_fold").unbind('click');
        $(".cs_cb_node_fold").click(function () {
            //0 展开，1 收缩
            var foldstatus = $(this).attr('data-fold');
            console.log($(this).parent());
            var parentCls = $(this).parent();
            if (parentCls.hasClass('ck-node-first')) {
                //一级
                if (foldstatus == 0) {
                    //收缩
                    parentCls.find('.ck-node-second').hide(100);
                    $(this).attr('src', '/images/fold.png');
                    $(this).attr('data-fold', 1);
                }
                else {
                    parentCls.find('.ck-node-second').show(100);
                    $(this).attr('src', '/images/unfold.png');
                    $(this).attr('data-fold', 0);
                }
            }
            else if (parentCls.hasClass('ck-node-second')) {
                //二级
                if (foldstatus == 0) {
                    //收缩
                    parentCls.find('.ck-node-third').hide(200);
                    $(this).attr('src', '/images/fold.png');
                    $(this).attr('data-fold', 1);
                }
                else {
                    parentCls.find('.ck-node-third').show(200);
                    $(this).attr('src', '/images/unfold.png');
                    $(this).attr('data-fold', 0);
                }
            }
            form.render();
        });
    }

    var monitorNodeEvent = function (nodeList, nodeId) {

        $.each(nodeList, function (iFirst, itemFirst) {   //一级

            form.on('checkbox(lf_node_' + itemFirst.id + ')', function (dataFirst) {
                var checkFirstStatus = dataFirst.elem.checked;
                $('.ck-node-first-' + itemFirst.id + ' .ck-node-second input:checkbox').each(function () {
                    $(this).prop('checked', checkFirstStatus);
                });

                form.render();
            });

            $.each(itemFirst.children, function (iSecond, itemSecond) {   //二级
                form.on('checkbox(lf_node_' + itemSecond.id + ')', function (dataSecond) {
                    var checkSecondStatus = dataSecond.elem.checked;
                    $('.ck-node-second-' + itemSecond.id + ' .ck-node-third input:checkbox').each(function () {
                        $(this).prop('checked', checkSecondStatus);
                    });

                    firstNodeCheckSetting(itemFirst.id);

                    form.render();
                });

                $.each(itemSecond.children, function (iThird, itemThird) {   //三级
                    form.on('checkbox(lf_node_' + itemThird.id + ')', function (dataThird) {
                        console.log(dataThird);
                        var checked = dataThird.elem.checked;
                        //第一个必须选中,如果第一个取消选中，则其他也需要取消
                        var thirdFirstNode = $('.ck-node-second-' + itemSecond.id + ' .ck-node-third input:checkbox[value="' + itemSecond.children[0].id + '"]');
                        if (checked) {
                            thirdFirstNode.prop('checked', true);
                        }
                        else {
                            if (!thirdFirstNode.prop('checked')) {
                                //取消所有三级选项
                                $('.ck-node-second-' + itemSecond.id + ' .ck-node-third input:checkbox').each(function () {
                                    $(this).prop('checked', false);
                                });
                            }
                        }

                        //三级选中总个数
                        var checkedThirdCount = $('.ck-node-second-' + itemSecond.id + ' .ck-node-third input[type="checkbox"]:checked').length;
                        $('#' + nodeId + ' input:checkbox[value="' + itemSecond.id + '"]').prop('checked', checkedThirdCount > 0);
                        //选中一级节点
                        firstNodeCheckSetting(itemFirst.id);

                        form.render();
                    });

                });
            });

        })

    }

    var firstNodeCheckSetting = function (firstNodeId) {
        //一级节点下面选中节点个数
        var checkedCount = $('.ck-node-first-' + firstNodeId + ' .ck-node-second input[type="checkbox"]:checked').length;
        $('.ck-node-first-' + firstNodeId + ' input:checkbox[value="' + firstNodeId + '"]').prop('checked', checkedCount > 0);
    }

    //输出
    exports('checkBoxUtil', checkBoxUtil);
});