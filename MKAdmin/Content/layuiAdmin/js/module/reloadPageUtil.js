﻿//页面跳转再次加载数据
layui.define(['jquery'], function (exports) {
    var $ = layui.jquery;

    var reloadPageUtil = {
        reloadPageData: function (id) {
            //刷新指定iframe
            parent.frames['menu_iframe_' + id].contentDocument.location.reload();
        }
    }
    exports('reloadPageUtil', reloadPageUtil);
});
