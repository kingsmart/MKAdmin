﻿//tab切换 
layui.define(['jquery', 'element', 'reloadPageUtil'], function (exports) {
    var $ = layui.jquery;
    var element = layui.element;
    var mainLayout = $('#main-layout', window.parent.document);
    var reloadPageUtil = layui.reloadPageUtil;

    var switchTab = {
        /*打开新的tab*/
        openTab: function (elem) {
            //debugger;
            var parameter = {
                id: 0,
                url: '',
                text: 'tab测试',
            }
            $.extend(parameter, elem);

            if (parameter == null || parameter.id <= 0) {
                return;
            }

            var id = parameter.id;
            console.log(id)
            var lay_this = $('.layui-nav-child', window.parent.document).find('a')

            $.each(lay_this, function (i, item) {//切换到左边菜单选中
                var currentId = $(item).attr('data-id')
                if (id == currentId) {
                    $('.layui-nav-child', window.parent.document).find('a').parents('.layui-nav-item').removeClass('layui-nav-itemed')
                    $('.layui-nav-child', window.parent.document).find('a').parents('dd').removeClass('layui-this')
                    $('.layui-nav-child', window.parent.document).find('a').eq(i).parents('.layui-nav-item').addClass('layui-nav-itemed')
                    $('.layui-nav-child', window.parent.document).find('a').eq(i).parents('dd').addClass('layui-this')

                }
            })
            var url = parameter.url;
            var text = parameter.text;
            if (!url) {
                return;
            }
            var isActive = $('.main-layout-tab .layui-tab-title', window.parent.document).find("li[lay-id=" + id + "]");
            if (isActive.length > 0) {
                //切换到选项卡
                element.tabChange('tab', id);
                reloadPageUtil.reloadPageData(id);

                //alert(1111);
            } else {
                //alert(2222);
                element.tabAdd('tab', {
                    title: text,
                    content: '<iframe src="' + url + '" id="menu_iframe_' + id + '" name="iframe' + id + '" class="iframe" framborder="0" data-id="' + id + '" scrolling="auto" width="100%"  height="100%"></iframe>',
                    id: id
                });
                element.tabChange('tab', id);
            }



            //切换到选项卡，左边菜单切换
            $('.layui-tab-title li', window.parent.document).click(function () {
                var tab_lay_this = $(this, window.parent.document).attr('lay-id')
                var lay_li_ln = $('.layui-nav-child', window.parent.document).find('a')
                var tab_lay_ln = $('.layui-tab-title li', window.parent.document)

                $.each(lay_li_ln, function (i, item) {
                    var currentId = $(item).attr('data-id')
                    if (tab_lay_this == currentId) {
                        $('.layui-nav-child', window.parent.document).find('a').parents('.layui-nav-item').removeClass('layui-nav-itemed')
                        $('.layui-nav-child', window.parent.document).find('a').parents('dd').removeClass('layui-this')
                        $('.layui-nav-child', window.parent.document).find('a').eq(i).parents('.layui-nav-item').addClass('layui-nav-itemed')
                        $('.layui-nav-child', window.parent.document).find('a').eq(i).parents('dd').addClass('layui-this')
                    } else if (tab_lay_this == undefined) {
                        $('.layui-nav-child', window.parent.document).find('a').parents('.layui-nav-item').removeClass('layui-nav-itemed')
                        $('.layui-nav-child', window.parent.document).find('a').parents('dd').removeClass('layui-this')
                    }

                })

            })
            //切换到选项卡，删除tab
            mainLayout.removeClass('hide-side', window.parent.document);
            $('.main-layout-tab .layui-tab-close', window.parent.document).click(function () {
                var ln = $(this, window.parent.document).parents('li').attr('lay-id')
                var lay_id = $('.main-layout-tab', window.parent.document).find('.layui-this').attr('lay-id')
                var lay_this = $('.layui-nav-child', window.parent.document).find('a')

                $.each(lay_this, function (i, item) {
                    var currentId = $(item).attr('data-id')
                    if (lay_id == currentId) {
                        $('.layui-nav-child', window.parent.document).find('a').parents('.layui-nav-item').removeClass('layui-nav-itemed')
                        $('.layui-nav-child', window.parent.document).find('a').parents('dd').removeClass('layui-this')
                        $('.layui-nav-child', window.parent.document).find('a').eq(i).parents('.layui-nav-item').addClass('layui-nav-itemed')
                        $('.layui-nav-child', window.parent.document).find('a').eq(i).parents('dd').addClass('layui-this')
                    } else if (lay_id == undefined) {
                        $('.layui-nav-child', window.parent.document).find('a').parents('.layui-nav-item').removeClass('layui-nav-itemed')
                        $('.layui-nav-child', window.parent.document).find('a').parents('dd').removeClass('layui-this')
                    }

                })

            })
        }
    };

    //输出test接口
    exports('switchTab', switchTab);
});
