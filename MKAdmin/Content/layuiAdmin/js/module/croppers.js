/*!
 * Cropper v3.0.0
 */

//layui.config({
//    base: '/static/cropper/' //layui自定义layui组件目录
//}).
layui.define(['jquery', 'layer', 'cropper'], function (exports) {
    var $ = layui.jquery
        , layer = layui.layer;
    var html = "<link rel=\"stylesheet\" href=\"/content/layuiadmin/css/cropper.css\">\n" +
                "<div class=\"layui-fluid showImgEdit\" style=\"display: none\">\n" +
                "    <div class=\"\">\n" +
                "        <div class=\"layui-input-inline layui-btn-container\" style=\"width: auto;\">\n" +
                "            <label for=\"cropper_avatarImgUpload\" class=\"layui-btn layui-btn-cropper layui-btn-primary\">\n" +
                "                <i class=\"layui-icon\">&#xe67c;</i>选择图片\n" +
                "            </label>\n" +
                "            <input class=\"layui-upload-file\" id=\"cropper_avatarImgUpload\" type=\"file\" value=\"选择图片\" name=\"file\">\n" +
                "        </div>\n" +
                "    </div>\n" +
                "    <div class=\"layui-row layui-col-space15\">\n" +
                "        <div class=\"layui-col-xs9\">\n" +
                "            <div class=\"readyimg\" style=\"height:450px;background-color: rgb(247, 247, 247);\">\n" +
                "                <img src=\"\" >\n" +
                "            </div>\n" +
                "        </div>\n" +
                "        <div class=\"layui-col-xs3\">\n" +
                "            <div class=\"img-preview\" style=\"width:200px;height:200px;overflow:hidden\">\n" +
                "            </div>\n" +
                "            <div class=\"img-preview-size\" style='text-align: center;margin-top: 5px;color: #f00;visibility:hidden'>当前尺寸:<span class='avatar-preview-size'></span></div>" +
                "            <div style=\"margin-top:186px\">\n" +
                "                 <button class=\"layui-btn layui-btn-cropper layui-btn-fluid\" cropper-event=\"confirmSave\" type=\"button\"> 保存修改</button>\n" +
                "             </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "\n" +
                "</div>";
    var obj = {
        render: function (e) {
            var uploadDialogIndex = 0;
            $('body').append(html);
            var self = this,
                elem = e.elem,
                saveW = e.saveW,
                saveH = e.saveH,
                mark = e.mark,
                area = ['900px', '550px'],
                url = e.url,
                data = e.data,
                done = e.done;

            var content = $('.showImgEdit')
                , image = $(".showImgEdit .readyimg img")
                , preview = '.showImgEdit .img-preview'
                , file = $(".showImgEdit input[name='file']")
                , options = { aspectRatio: mark, preview: preview, viewMode: 1 };

            $(elem).on('click', function () {
                uploadDialogIndex = layer.open({
                    type: 1
                    , content: content
                    , area: area
                    , success: function () {
                        image.cropper(options);
                    }
                    , cancel: function (index) {
                        layer.close(index);
                        //$(document).off("change", "#cropper_avatarImgUpload");
                        image.cropper('destroy');
                        //$(".showImgEdit input[name='file']").unbind();
                    }
                });
            });

            //文件选择
            file.change(function () {
                var r = new FileReader();
                var f = this.files[0];
                r.readAsDataURL(f);
                r.onload = function (e) {
                    image.cropper('destroy').attr('src', this.result).cropper(options);

                    //延时一点时间获取尺寸
                    setTimeout(function () {
                        $(".img-preview-size").css('visibility', 'visible');
                        image.cropper('showPreviewSize');
                    }, 200)
                };
            });

            $(".layui-btn-cropper").on('click', function () {
                var event = $(this).attr("cropper-event");
                //监听确认保存图像
                if (event === 'confirmSave') {
                    image.cropper("getCroppedCanvas", {
                        //width: saveW,
                        //height: saveH
                    }).toBlob(function (blob) {
                        var formData = new FormData();
                        formData.append('file', blob, 'head.jpg');
                        formData.append('uploadfile_ant', data.uploadfile_ant);

                        layer.load(3, {
                            shade: [0.5, '#fff'] //0.1透明度的白色背景
                        });
                        $.ajax({
                            method: "post",
                            url: url, //用于文件上传的服务器端请求地址
                            data: formData,
                            processData: false,
                            contentType: false,
                            success: function (result) {
                                return done(result);
                            },
                            complete: function (res) {
                                layer.close(uploadDialogIndex);
                                layer.closeAll('loading');
                            },
                        });
                    });
                    //监听旋转
                } else if (event === 'rotate') {
                    var option = $(this).attr('data-option');
                    image.cropper('rotate', option);
                    //重设图片
                } else if (event === 'reset') {
                    image.cropper('reset');
                }
            });
        }

    };
    exports('croppers', obj);
});