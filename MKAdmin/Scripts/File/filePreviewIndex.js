﻿//在线预览
layui.use(['jquery', 'dialog', 'ajaxUtil'], function () {
    var $ = layui.jquery
        , dialog = layui.dialog
        , ajaxutil = layui.ajaxUtil;

    //预览
    $('.clsOnlinePreview').on('click', function () {
        var catalogName = $(this).attr('data-catalogname');

        var catalogSp = catalogName.split('/');
        var fileName = catalogSp[catalogSp.length - 1];
        ajaxutil.request({
            url: "/file/preview/onliepreview?catalogName=" + catalogName
            , type: "GET"
            , success: function (res) {
                if (res.status) {
                    dialog.openUrl({
                        url: res.data.PreviewFileUrl
                        , title: '预览文件[' + fileName + ']'
                        , width: 700
                        , height: 500
                    })
                }
                else {
                    dialog.alert.fail('生成预览失败');
                }
            }
        });
    });
});