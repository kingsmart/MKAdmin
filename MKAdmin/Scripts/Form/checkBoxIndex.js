﻿//表格管理 - 基本表格
layui.use(['form', 'jquery', 'dialog', 'ajaxUtil', 'permissionUtil', 'checkBoxUtil'], function () {
    var $ = layui.jquery
        , dialog = layui.dialog
        , ajaxutil = layui.ajaxUtil
        , form = layui.form
        , permissionUtil = layui.permissionUtil
        , checkBoxUtil = layui.checkBoxUtil;
    //permissionUtil.setting(100000);

    //
    //var nodeList = [
    //    { id: 1000, parentId: 0, name: '表格管理', isParent: true, checked: true }
    //    , { id: 100010, parentId: 1000, name: '基本表格', isParent: true, checked: true }
    //    , { id: 100020, parentId: 1000, name: '复选框表格', isParent: true, checked: true }
    //    , { id: 100030, parentId: 1000, name: '设置单元格样式', isParent: true, checked: false }
    //    , { id: 10001010, parentId: 100010, name: '查询', isParent: false, checked: true }
    //    , { id: 10001020, parentId: 100010, name: '新增', isParent: false, checked: false }
    //    , { id: 1010, parentId: 0, name: '表单', isParent: true, checked: true }];

    var nodeList = [
        {
            id: 1000, name: '表格管理', checked: true,
            children: [
                {
                    id: 100010, name: '基本表格', checked: true, children: [
                        { id: 10001010, name: '显示菜单', checked: true }
                        , { id: 10001020, name: '新增', checked: false }
                        , { id: 10001030, name: '编辑', checked: true }
                        , { id: 10001040, name: '删除', checked: false }
                    ]
                }
                , {
                    id: 100020, name: '复选框表格', checked: false, children: [
                        { id: 10002010, name: '显示菜单', checked: false }
                        , { id: 10002020, name: '获取选中编号', checked: false }
                    ]
                }
                , {
                    id: 100030, name: '设置单元格样式', checked: true, children: [
                        { id: 10003010, name: '显示菜单', checked: true }]
                }]
        }
        , {
            id: 1010, name: '表单', checked: false,
            children: [
                {
                    id: 101010, name: '下拉框', checked: false, children: [
                        { id: 10101010, name: '显示菜单', checked: false }
                    ]
                }
                , {
                    id: 101020, name: 'CheckBox复选框', checked: false, children: [
                        { id: 10102010, name: '显示菜单', checked: false }
                        , { id: 10102020, name: '获取选中值', checked: false }
                    ]
                }]
        }];

    checkBoxUtil.init({
        nodeId: 'dv_form_checkbox_head'
        , nodeList: nodeList
    });

    //获取选中值
    $('#btn_FormCheckBox_GetValue').on('click', function () {
        var checkedValue = checkBoxUtil.getCheckedVal({
            nodeId: 'dv_form_checkbox_head'
        });
        alert(checkedValue);
    });
});