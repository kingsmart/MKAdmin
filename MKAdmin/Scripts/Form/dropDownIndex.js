﻿//表格管理 - 基本表格
layui.use(['form', 'jquery', 'dialog', 'ajaxUtil', 'permissionUtil'], function () {
    var $ = layui.jquery,
        dialog = layui.dialog,
        ajaxutil = layui.ajaxUtil,
        form = layui.form,
        permissionUtil = layui.permissionUtil;
    //permissionUtil.setting(100000);

    //获取值
    $('#btn_FormDropDown_GetValue').on('click', function () {
        var value = $("#sn_form_dropdown_select1").val();
        if (value == '') {
            dialog.alert.msg('请选择一项');
        }
        else {
            dialog.alert.msg(value);
        }
    });

    //设置值
    $('#btn_FormDropDown_SetValue').on('click', function () {
        $("#sn_form_dropdown_select1").val(1);

        //更新 lay-filter="lf_form_node" 所在容器内的全部表单状态
        form.render('select', 'lf_form_node');
    });

    //下拉框选择监听
    form.on('select(lf_form_prov)', function (data) {
        //console.log(data);
        var value = data.value;
        //这里模拟数据，可以动态请求接口返回数据
        if (value == 1) {
            $("#dp_Form_City").empty();
            $("#dp_Form_City").append("<option value=\"\">请选择市</option><option value=111>广州市</option><option value=112>深圳市</option>");
        }
        else if (value == 2) {
            $("#dp_Form_City").empty();
            $("#dp_Form_City").append("<option value=\"\">请选择市</option><option value=111>南昌市</option><option value=112>景德镇市</option>");
        }

        //表单重新渲染，要不然添加完显示不出来新的option
        form.render('select', 'lf_form_node');
    })
});