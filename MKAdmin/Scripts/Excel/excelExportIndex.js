﻿//导入Excel文件
layui.use(['form', 'jquery', 'dialog', 'table', 'laypage', 'ajaxUtil', 'permissionUtil'], function () {
    var table = layui.table
        , laypage = layui.laypage
        , $ = layui.jquery
        , dialog = layui.dialog
        , ajaxutil = layui.ajaxUtil
        , form = layui.form
        , permissionUtil = layui.permissionUtil;
    //permissionUtil.setting(100000);

    var whereParam = {
        name: ''
    };

    //表格渲染
    table.render({
        id: 'cn_GridExcelExport'
        , elem: '#grid_ExcelExport'
        , method: 'post'
        , url: '/table/basictable/list'
        //启动分页
        , page: true
        , height: 'full-100'
        , loading: true
        , text: { none: '未查询到数据' }
        , limits: [20, 50, 100]
        //禁用前端自动排序
        , autoSort: false
        , where: {
            name: ''
        }
        , request: {
            pageName: 'pageIndex'
            , limitName: 'pageSize'
        }
        , cols: [[
            { field: 'EmployeeId', width: 80, title: '编号' }
            , { field: 'EmployeeName', width: 150, title: '姓名' }
            , { field: 'EmployeeAge', title: '年龄', width: 100 }
            , { field: 'Education', width: 180, title: '学历' }
            , { field: 'ContactPhone', width: 150, title: '联系方式' }
        ]]
    });

    var loadData = function (param, pageIndex) {
        //执行重载
        table.reload('cn_GridExcelExport', {
            page: {
                curr: pageIndex //刷新指定页
            }
            , where: param
        });
    }

    //筛选
    $('#btn_ExcelExport_Search').on('click', function () {
        whereParam.name = $('#txt_ExcelExport_Name').val();

        loadData({
            name: whereParam.name
        }, 1)
    });

    //导出Excel
    $('#btn_ExcelExport_Data').on('click', function () {

        ajaxutil.request({
            url: '/excel/export/export'
            , type: 'Post'
            , data: {
                name: whereParam.name
            },
            success: function (res) {
                //alert(res);
                if (res.status) {
                    console.log(res);
                    $("#btn_ExcelExport_Export").attr("href", '/home/downloadconfigfile?key=Excel&catalog=' + res.data.catalog + '&fileName=' + res.data.fileName + '&type=0');
                    //触发a标签点击事件
                    document.getElementById("btn_ExcelExport_Export").click();
                    //window.open('/home/downloadconfigfile?key=Excel&catalog=' + res.data.catalog + '&fileName=' + res.data.fileName + '&type=0');
                }
                else {
                    dialog.alert.fail(res.msg);
                }
            }
        });
    });
});