﻿//表格管理 - 基本表格
layui.use(['form', 'jquery', 'laydate', 'layer', 'dialog', 'table', 'laypage', 'ajaxUtil'
    , 'permissionUtil', 'switchTab', 'tableCheckBoxUtil'], function () {
        var table = layui.table,
            laypage = layui.laypage,
            $ = layui.jquery,
            layer = layui.layer,
            dialog = layui.dialog,
            ajaxutil = layui.ajaxUtil,
            laydate = layui.laydate,
            form = layui.form,
            permissionUtil = layui.permissionUtil,
            switchTab = layui.switchTab,
            tableCheckBoxUtil = layui.tableCheckBoxUtil;

        //permissionUtil.setting(100000);

        //表格渲染
        table.render({
            id: 'cn_GridCheckBoxTable'
            , elem: '#grid_CheckBoxTable'
            , method: 'post'
            , url: '/table/basictable/list'
            //启动分页
            , page: true
            , height: 'full-100'
            , loading: true
            , text: { none: '未查询到数据' }
            , limit: 20
            , limits: [20, 50, 100]
            //禁用前端自动排序
            , autoSort: false
            , where: {
                name: ''
            }
            , request: {
                pageName: 'pageIndex'
                , limitName: 'pageSize'
            }
            , cols: [[
                { checkbox: true }
                , { field: 'EmployeeId', width: 80, title: '编号' }
                , { field: 'EmployeeName', width: 150, title: '姓名' }
                , { field: 'EmployeeAge', title: '年龄', width: 100 }
                , { field: 'Education', width: 180, title: '学历' }
                , { field: 'ContactPhone', width: 150, title: '联系方式' }
            ]]
            , done: function (data) {
                console.log(data);
                //初始化表格行选中状态
                tableCheckBoxUtil.checkedDefault({
                    gridId: 'cn_GridCheckBoxTable'
                    , fieldName: 'EmployeeId'
                });
            }
        });

        //初始化分页设置
        tableCheckBoxUtil.init({
            gridId: 'cn_GridCheckBoxTable'
            , filterId: 'ft_CheckBoxTable'
            , fieldName: 'EmployeeId'
        });

        //添加
        $('#btn_CheckBoxTableAdd').on('click', function () {
            dialog.open({
                dialogId: 'dialog_BasicTable_Add'
                , width: 600
                , height: 330
                , title: '添加'
                , ok: function () {
                    addEmployee();
                }
                , cancel: function () {

                }
            });
        });

        //筛选
        $('#btn_CheckBoxSearch').on('click', function () {
            //执行重载
            table.reload('cn_GridCheckBoxTable', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                , where: {
                    name: $('#txt_BasicIndex_Name').val()
                }
            });
        });

        //获取选中编号
        $("#btn_CheckBoxTableGetValue").on('click', function () {
            var selectedData = tableCheckBoxUtil.getValue({
                gridId: 'cn_GridCheckBoxTable'
            });

            alert(selectedData);
        });

    });