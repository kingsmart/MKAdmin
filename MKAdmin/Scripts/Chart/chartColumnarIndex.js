﻿//表格管理 - 基本表格
layui.use(['jquery', 'table', 'ajaxUtil', 'permissionUtil', 'switchTab'], function () {
    var table = layui.table,
        $ = layui.jquery,
        ajaxutil = layui.ajaxUtil,
        permissionUtil = layui.permissionUtil,
        switchTab = layui.switchTab;
    //permissionUtil.setting(100000);
    var myChart;

    var loadBasicLine = function () {
        // 基于准备好的dom，初始化echarts实例
        if (myChart !== null && myChart !== undefined) {
            myChart.clear();
        }
        myChart = echarts.init(document.getElementById('sn_dv_chartColumnar_main'));

        var option = {
            color: ['#3398DB'],
            title: {
                text: '员工学历人数统计'
            },
            tooltip: {
                trigger: 'axis'
                , formatter: function (params) {
                    //console.log(params);
                    var xText = params[0].name;
                    var yValue = params[0].value;
                    var marker = params[0].marker;
                    return xText + '<br/> ' + marker + yValue + '人';
                }
            },
            xAxis: {
                type: 'category',
                data: ['初中', '高中', '大专', '本科', '博士']
            },
            yAxis: {
                type: 'value'
            },
            series: [{
                data: [820, 932, 901, 934, 1290]
                , type: 'bar'
            }]
        };

        myChart.setOption(option, true);
    }

    loadBasicLine();
    $('#btn_ChartColumnar_Refresh').click(function () {
        loadBasicLine();
    });


});