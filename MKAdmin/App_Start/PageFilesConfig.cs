﻿using MKAdmin.ToolKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKAdmin.Web.App_Start
{
    public class PageJsFilesConfig
    {
        static string v = AppSetting.ProductVersion;

        public static string LayuiJs = "/Content/layuiAdmin/layui/layui.js?v=" + v;
        public static string LayuiElement = "/Content/layuiAdmin/layui/lay/modules/element.js?v=" + v;

        public static string LayuiCommon = "/Content/layuiAdmin/js/common.js?v=" + v;
        public static string LayuiMain = "/Content/layuiAdmin/js/main.js?v=" + v;
        public static string Echarts = "/Content/echarts/echarts.min.js?v=" + v;
        public static string Login = "/Scripts/Login/login.js?v=" + v;
        public static string CommonSys = "/Scripts/Common/commonSys.js?v=" + v;
        public static string Jquery = "/Scripts/jquery-1.10.2.min.js?v=" + v;
        public static string JqCookie = "/Scripts/jquery.cookie.min.js?v=" + v;

        public static string WelcomeIndex = "/Scripts/Home/welcome.js?v=" + v;
        public static string HomeIndex = "/Scripts/Home/homeIndex.js?v=" + v;

        //HighLighter
        public static string HighLighterShCore = "/content/syntaxhighlighter/scripts/shCore.js?v=" + v;
        public static string HighLighterShBrushBash = "/content/syntaxhighlighter/scripts/shBrushBash.js?v=" + v;
        public static string HighLighterShBrushCss = "/content/syntaxhighlighter/scripts/shBrushCss.js?v=" + v;
        public static string HighLighterShBrushCSharp = "/content/syntaxhighlighter/scripts/shBrushCSharp.js?v=" + v;
        public static string HighLighterShBrushJScript = "/content/syntaxhighlighter/scripts/shBrushJScript.js?v=" + v;
        public static string HighLighterShBrushPlain = "/content/syntaxhighlighter/scripts/shBrushPlain.js?v=" + v;
        public static string HighLighterShBrushSql = "/content/syntaxhighlighter/scripts/shBrushSql.js?v=" + v;
        public static string HighLighterShBrushXml = "/content/syntaxhighlighter/scripts/shBrushXml.js?v=" + v;
        //public static string HighLighter = "/Content/syntaxHighLighter/shCore.js?v=" + v;
        //public static string HighLighterHtml = "/Content/syntaxHighLighter/shCore.js?v=" + v;

        //表格控件
        public static string BasicTableIndex = "/Scripts/Table/basicTableIndex.js?v=" + v;
        public static string CheckBoxTableIndex = "/Scripts/Table/checkBoxTableIndex.js?v=" + v;
        public static string CheckBoxTableMultiIndex = "/Scripts/Table/checkBoxTableMultiIndex.js?v=" + v;
        public static string CellStyleIndex = "/Scripts/Table/cellStyleIndex.js?v=" + v;

        //表单
        public static string FormDropDownIndex = "/Scripts/Form/dropDownIndex.js?v=" + v;
        public static string FormCheckBoxIndex = "/Scripts/Form/checkBoxIndex.js?v=" + v;

        //图表
        public static string ChartLineIndex = "/Scripts/Chart/chartLineIndex.js?v=" + v;
        public static string ChartColumnarIndex = "/Scripts/Chart/chartColumnarIndex.js?v=" + v;
        public static string ChartPieIndex = "/Scripts/Chart/chartPieIndex.js?v=" + v;
            
        //Excel操作
        public static string ExcelImportIndex = "/Scripts/Excel/excelImportIndex.js?v=" + v;
        public static string ExcelExportIndex = "/Scripts/Excel/excelExportIndex.js?v=" + v;

        //文件管理
        public static string FilePreviewIndex = "/Scripts/File/filePreviewIndex.js?v=" + v;
        public static string FileCropIndex = "/Scripts/File/fileCropIndex.js?v=" + v;
        public static string FileUploadIndex = "/Scripts/File/fileUploadIndex.js?v=" + v;

        //通讯管理
        public static string CommunicateChatIndex = "/Scripts/Communicate/communicateChatIndex.js?v=" + v;
        public static string CommunicateEmailIndex = "/Scripts/Communicate/communicateEmailIndex.js?v=" + v;

        //表情
        public static string SinaFaceAndEffec = "/Scripts/Common/sinaFaceAndEffec.js?v=" + v;

        //pdf预览
        public static string PdfObjdec = "/Content/pdfobject/pdfobject.js";

        public static readonly string SignalR = "/Scripts/jquery.signalR-2.4.0.js";
        public static readonly string Hubs = "/signalr/hubs";
    }

    public class PageCssFilesConfig
    {
        static string v = AppSetting.ProductVersion;

        public static string LayuiCss = "/Content/layuiAdmin/layui/css/layui.css?v=" + v;
        public static string LayuiAdmin = "/Content/layuiAdmin/css/admin.css?v=" + v;
        public static string Login = "/Content/layuiAdmin/css/login.css?v=" + v;
        public static string UserLogin = "/Styles/Login/login.css?v=" + v;
        public static string Home = "/Styles/Common/home.css?v=" + v;
        public static string CommonSys = "/Styles/Common/commonSys.css?v=" + v;

        public static string WelcomeIndex = "/Styles/Home/welcome.css?v=" + v;

        //HighLighter
        public static string HighLighterShCore = "/content/syntaxhighlighter/styles/shCore.css?v=" + v;
        public static string HighLighterShThemeDefault = "/content/syntaxhighlighter/styles/shThemeDefault.css?v=" + v;

        //表格控件
        public static string BasicTableIndex = "/Styles/Table/basicTableIndex.css?v=" + v;
        public static string CheckBoxTableIndex = "/Styles/Table/checkBoxTableIndex.css?v=" + v;
        public static string CellStyleIndex = "/Styles/Table/cellStyleIndex.css?v=" + v;

        //表单
        public static string FormDropDownIndex = "/Styles/Form/dropDownIndex.css?v=" + v;
        public static string FormCheckBoxIndex = "/Styles/Form/checkBoxIndex.css?v=" + v;

        //Excel操作
        public static string ExcelImportIndex = "/Styles/Excel/excelImportIndex.css?v=" + v;
        public static string ExcelExportIndex = "/Styles/Excel/excelExportIndex.css?v=" + v;

        //文件管理
        public static string FilePreviewIndex = "/Styles/File/filePreviewIndex.css?v=" + v;
        public static string FileCropIndex = "/Styles/File/fileCropIndex.css?v=" + v;
        public static string FileUploadIndex = "/Styles/File/fileUploadIndex.css?v=" + v;

        //通讯管理
        public static string CommunicateChatIndex = "/Styles/Communicate/communicateChatIndex.css?v=" + v;
        public static string CommunicateEmailIndex = "/Styles/Communicate/communicateEmailIndex.css?v=" + v;

        //表情
        public static string FacePanel = "/Styles/Common/facePanel.css?v=" + v;
        public static string Cropper = "/Content/layuiAdmin/css/cropper.css?v=" + v;

        //聊天样式
        public static string ChartStyle = "/Styles/Common/chart-style.css?v=" + v;
        public static string Style = "/Styles/Common/style.css?v=" + v;
        public static string MassStyle = "/Styles/Common/mass-style.css?v=" + v;
    }

    public class PageViewFilesConfig
    {
        //表格控件
        public static string BasicTableIndex = "/Views/Table/BasicTable/Index.cshtml";
        public static string BasicTableAdd = "/Views/Table/BasicTable/Dialog/Add.cshtml";
        public static string CheckBoxTableIndex = "/Views/Table/CheckBoxTable/Index.cshtml";
        public static string CheckBoxTableMultiIndex = "/Views/Table/CheckBoxTable/MultiIndex.cshtml";
        public static string CellStyleIndex = "/Views/Table/CellStyle/Index.cshtml";

        //表单
        public static string FormDropDownIndex = "/Views/Form/DropDown/Index.cshtml";
        public static string FormCheckBoxIndex = "/Views/Form/CheckBox/Index.cshtml";

        //图表
        public static string ChartLineIndex = "/Views/Chart/Line/Index.cshtml";
        public static string ChartColumnarIndex = "/Views/Chart/Columnar/Index.cshtml";
        public static string ChartPieIndex = "/Views/Chart/Pie/Index.cshtml";

        //Excel操作
        public static string ExcelImportIndex = "/Views/Excel/Import/Index.cshtml";
        public static string ExcelExportIndex = "/Views/Excel/Export/Index.cshtml";

        //文件管理
        public static string FileUploadIndex = "/Views/File/Upload/Index.cshtml";
        public static string FilePreviewIndex = "/Views/File/Preview/Index.cshtml";
        public static string FileCropIndex = "/Views/File/Crop/Index.cshtml";

        //通讯管理
        public static string CommunicateChatIndex = "/Views/Communicate/Chat/Index.cshtml";
        public static string CommunicateEmailIndex = "/Views/Communicate/Email/Index.cshtml";

        public static string Home = "/Views/Home/Index.cshtml";
        public static string Welcome = "/Views/Home/Welcome.cshtml";
        //public static string BasicGrid = "/Views/GridMgr/BasicGrid.cshtml";
        //public static string BasicGridAdd = "/Views/GridMgr/Dialog/Add.cshtml";
        public static string Login = "/Views/Login/Index.cshtml";

        //首页
        public static string HomeIndex = "/Areas/Home/Views/Home/Index.cshtml";
    }
}