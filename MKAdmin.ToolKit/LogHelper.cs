﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.ToolKit
{
    public static class LogHelper
    {
        const string LOG4NET_CONFIG_FILE_NAME = "log4net.config";

        private static ILog log_info;
        /// <summary>
        /// 调试信息日志
        /// </summary>
        private static ILog LOG_INFO
        {
            get
            {
                if (log_info == null)
                {
                    Init();
                }
                return log_info;
            }
        }

        private static ILog log_error;
        /// <summary>
        /// 异常信息日志
        /// </summary>
        private static ILog LOG_ERROR
        {
            get
            {
                if (log_error == null)
                {
                    Init();
                }
                return log_error;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private static void Init()
        {
            string fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, LOG4NET_CONFIG_FILE_NAME);
            if (!File.Exists(fileName))
                WriteLog4NetConfig(fileName);

            FileInfo fi = new FileInfo(fileName);

            log4net.Config.XmlConfigurator.Configure(fi);

            log_error = LogManager.GetLogger("logerror");
            log_info = LogManager.GetLogger("loginfo");
        }

        /// <summary>
        /// 写出log4net配置信息文件
        /// </summary>
        /// <param name="fileName">输出文件名称</param>
        private static void WriteLog4NetConfig(string fileName)
        {
            FileStream fs = null;
            Stream xmlStream = null;
            byte[] bytsXml;
            int lenByts;

            try
            {
                Assembly asmCurrent = Assembly.GetExecutingAssembly();
                xmlStream = asmCurrent.GetManifestResourceStream(typeof(LogHelper).Namespace
                    + "." + LOG4NET_CONFIG_FILE_NAME);
                bytsXml = new byte[xmlStream.Length];
                using (fs = File.Create(fileName))
                {
                    while ((lenByts = xmlStream.Read(bytsXml, 0, bytsXml.Length)) > 0)
                    {
                        //向文件中写信息
                        fs.Write(bytsXml, 0, lenByts);
                        fs.Flush();
                    }
                }
            }
            finally
            {
                if (fs != null)
                    fs.Close();

                if (xmlStream != null)
                    xmlStream.Close();
            }
        }

        public static void Error(string message)
        {
            LOG_ERROR.Error(message);
        }

        public static void Error(string message, Exception ex)
        {
            LOG_ERROR.Error(message, ex);
        }

        public static void Info(string message)
        {
            LOG_INFO.Info(message);
        }
        public static void Info(string message, Exception ex)
        {
            LOG_INFO.Info(message, ex);
        }
    }
}
