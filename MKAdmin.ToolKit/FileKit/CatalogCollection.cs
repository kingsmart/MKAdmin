﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.ToolKit.FileKit
{
    public class CatalogCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// 初始
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new CatalogElement();
        }

        /// <summary>
        /// Key
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((CatalogElement)element).Key;
        }

        /// <summary>
        /// 集合类型
        /// </summary>
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        /// <summary>
        /// 节点名（
        /// </summary>
        protected override string ElementName
        {
            get
            {
                return "Catalog";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public CatalogElement this[object key]
        {
            get
            {
                return (CatalogElement)BaseGet(key);
            }
        }
    }
}
