﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.ToolKit
{
    public static class DataTableHelper
    {
        /// <summary>
        /// DataTable转换成List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static List<T> ToList<T>(this DataTable dt) where T : class, new()
        {
            List<T> oList = new List<T>();
            Type type = typeof(T);
            string tempName = string.Empty;
            foreach (DataRow dr in dt.Rows)
            {
                T t = new T();
                PropertyInfo[] propertys = t.GetType().GetProperties();
                foreach (PropertyInfo pi in propertys)
                {
                    tempName = pi.Name;
                    if (dt.Columns.Contains(tempName))
                    {
                        if (!pi.CanWrite) continue;
                        object value = dr[tempName];

                        if (value != DBNull.Value)
                        {
                            pi.SetValue(t, value, null);
                        }
                        else if (pi.PropertyType.Name.ToLower() == "string")
                        {
                            pi.SetValue(t, "", null);
                        }
                    }
                }
                oList.Add(t);
            }
            return oList;
        }

        /// <summary>
        /// DataTable转换成Info
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static T ToInfo<T>(this DataTable dt) where T : class, new()
        {
            T oInfo = new T();
            Type type = typeof(T);
            string tempName = string.Empty;
            //获取第一行数据
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dRow = dt.Rows[0];
                PropertyInfo[] propertys = oInfo.GetType().GetProperties();

                foreach (PropertyInfo pi in propertys)
                {
                    tempName = pi.Name;
                    if (dt.Columns.Contains(tempName))
                    {
                        if (!pi.CanWrite) continue;
                        object value = dRow[tempName];

                        if (value != DBNull.Value)
                        {
                            pi.SetValue(oInfo, value, null);
                        }
                        else if (pi.PropertyType.Name.ToLower() == "string")
                        {
                            pi.SetValue(oInfo, "", null);
                        }
                    }
                }
            }
            return oInfo;
        }

        /// <summary>
        /// DataTable转换成Info
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static T ToInfo<T>(this DataRow dRow) where T : class, new()
        {
            T oInfo = new T();
            Type type = typeof(T);
            string tempName = string.Empty;

            PropertyInfo[] propertys = oInfo.GetType().GetProperties();

            foreach (PropertyInfo pi in propertys)
            {
                tempName = pi.Name;
                if (!pi.CanWrite) continue;
                object value = dRow[tempName];
                if (value != DBNull.Value)
                    pi.SetValue(oInfo, value, null);
            }

            return oInfo;
        }

        #region DataTable更换列名
        /// <summary>
        /// DataTable更换列名
        /// </summary>
        /// <param name="dtList">被更换的数据源</param>
        /// <param name="propertyKeyNames">更换列名(字典类型)</param>
        /// <returns></returns>
        public static DataTable ReplaceHeader(this DataTable dtList, Dictionary<string, string> propertyKeyNames)
        {
            DataTable result = new DataTable();

            if (dtList != null && dtList.Rows.Count > 0)
            {
                //修改表头名
                foreach (var property in propertyKeyNames)
                {
                    result.Columns.Add(property.Value, dtList.Columns[property.Key].DataType);
                }

                //填充数据源内容
                for (int i = 0; i < dtList.Rows.Count; i++)
                {
                    ArrayList tempList = new ArrayList();
                    foreach (var property in propertyKeyNames)
                    {
                        object obj = dtList.Rows[i][property.Key];
                        tempList.Add(obj);
                    }

                    result.LoadDataRow(tempList.ToArray(), true);
                }
            }

            return result;
        }
        #endregion
    }
}
