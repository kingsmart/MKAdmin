﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.ToolKit
{
    public static class AppSetting
    {
        public static string ProductVersion = AppSettingsKit.GetValue("ProductVersion");
        public static string RedisConnection = AppSettingsKit.GetValue("RedisConnectionString");
        public static string ProjectDomain = AppSettingsKit.GetValue("ProjectDomain");
        public static string LocationApiDomain = AppSettingsKit.GetValue("LocationApiDomain");
    }
    public static class CacheNameSetting
    {

    }
}
