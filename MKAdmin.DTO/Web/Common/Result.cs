﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Common
{
    /// <summary>
    /// 通用返回结果
    /// </summary>
    public class Result
    {
        /// <summary>
        /// 返回状态, 默认为true
        /// </summary>
        public bool status { get; set; } = true;

        /// <summary>
        /// 返回消息
        /// </summary>
        public string msg { get; set; } = "操作成功";
    }

    /// <summary>
    /// 通用返回结果带数据
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Result<T> : Result
    {
        /// <summary>
        /// 返回数据
        /// </summary>
        public T data { get; set; }
    }
}
