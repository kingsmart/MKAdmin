﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Common
{
    /// <summary>
    /// 分页通用参数
    /// </summary>
    public class PagingParameter
    {
        /// <summary>
        /// 页数
        /// </summary>
        public int pageIndex { get; set; } = 1;
        /// <summary>
        /// 每页记录数
        /// </summary>
        public int pageSize { get; set; } = 20;
        /// <summary>
        /// 排序字段名称
        /// </summary>
        public string sortName { get; set; }
        /// <summary>
        /// 升序、降序
        /// </summary>
        public string sortOrder { get; set; }
    }
}
