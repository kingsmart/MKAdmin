﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Login
{
    public class LoginInfoParameter
    {
        public int UserId { get; set; }
        /// <summary>
        /// 登录方式 0:账号登录  1:扫码登录
        /// </summary>
        public int LoginType { get; set; }
        /// <summary>
        /// 登录账号
        /// </summary>
        public string OperatorNo { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string OperatorPwd { get; set; }
        /// <summary>
        /// 验证码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 错误信息键
        /// </summary>
        private const string _KeyValidateMessage = "ValidateMessage";
        /// <summary>
        /// KeyValidateMessage
        /// </summary>
        public string KeyValidateMessage
        {
            get { return _KeyValidateMessage; }
        }
        public bool IsRemember { get; set; }
    }
}
