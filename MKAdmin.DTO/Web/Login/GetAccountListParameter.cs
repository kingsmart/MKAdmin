﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Login
{
    public class GetAccountListParameter
    {
        /// <summary>
        /// 
        /// </summary>
        public int AgentId { get; set; }
        /// <summary>
        /// 是否包含当前账号
        /// </summary>
        public bool ContainCurrentAccount { get; set; }
        /// <summary>
        /// 账号状态
        /// </summary>
        public EmAccountStatusCode AccountStatusCode { get; set; } = EmAccountStatusCode.NormalOnly;
        /// <summary>
        /// 账号类型
        /// </summary>
        public EmAccountTypeCode AccountTypeCode { get; set; } = EmAccountTypeCode.Superior;
    }

    /// <summary>
    /// 账号类型
    /// </summary>
    public enum EmAccountTypeCode
    {
        /// <summary>
        /// 超级管理员
        /// </summary>
        Superior = 1000,
        /// <summary>
        /// 一级账号
        /// </summary>
        First = 1100,
        /// <summary>
        /// 二级账号
        /// </summary>
        Second = 1200,
        /// <summary>
        /// 客服账号
        /// </summary>
        Service = 1300,
        /// <summary>
        /// 超级管理员、一级账号、二级账号
        /// </summary>
        Manager = 1400,
        /// <summary>
        /// 超级管理员、一级账号、二级账号、客服账号
        /// </summary>
        All = 1500
    }
}
