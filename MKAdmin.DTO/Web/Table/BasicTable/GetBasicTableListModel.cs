﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Table.BasicTable
{
    public class GetBasicTableListModel
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public byte EmployeeSex { get; set; }
        public int EmployeeAge { get; set; }
        public int EmployeeHeight { get; set; }
        public string ContactPhone { get; set; }
        public string Education { get; set; }
        public DateTime EntryTime { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
