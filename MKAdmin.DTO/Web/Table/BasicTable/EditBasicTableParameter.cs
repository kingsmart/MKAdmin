﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Table.BasicTable
{
    public class EditBasicTableParameter
    {
        /// <summary>
        /// 员工编号
        /// </summary>
        public int employeeId { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string employeeName { get; set; }
        /// <summary>
        /// 年龄
        /// </summary>
        public int employeeAge { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public int employeeSex { get; set; }
        /// <summary>
        /// 身高
        /// </summary>
        public int employeeHeight { get; set; }
        /// <summary>
        /// 学历
        /// </summary>
        public string employeeEducation { get; set; }
        /// <summary>
        /// 联系方式
        /// </summary>
        public string employeePhone { get; set; }
    }
}
